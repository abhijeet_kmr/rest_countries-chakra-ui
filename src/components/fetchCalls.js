import axios from "axios";

export const getCountries = () => {
  return axios
    .get("https://restcountries.com/v3.1/all")
    .then((res) => res.data);
};

export const getCountryByCode = (code) => {
  return axios
    .get(`https://restcountries.com/v3.1/alpha/${code}`)
    .then((res) => res.data);
};
