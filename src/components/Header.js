import React, { Component } from "react";
import { Flex } from "@chakra-ui/react";
import { Text } from "@chakra-ui/react";

export default class Header extends Component {
  render() {
    return (
      <Flex height="4rem" bg="#fff" justify="space-between" align="center">
        <Text fontSize="3xl" fontWeight="bold" padding="0 5rem">
          Where in the World?
        </Text>
        <Text fontSize="3xl" fontWeight="bold" padding="0 5rem">
          Dark Mode
        </Text>
      </Flex>
    );
  }
}
