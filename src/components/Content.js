import React, { Component } from "react";
import { Box, Image, Flex } from "@chakra-ui/react";
import * as CountryAPI from "./fetchCalls";
import { Input } from "@chakra-ui/react";
import { Select } from "@chakra-ui/react";
import { Heading } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { Spinner } from "@chakra-ui/react";

export default class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countries: [],
      isLoading: true,
      isError: false,
      selectedValue: "",
      searchKeyWord: "",
      region: "All",
    };
  }

  componentDidMount() {
    CountryAPI.getCountries()
      .then((countries) => this.setState({ countries, isLoading: false }))
      .catch((err) => {
        this.setState({
          isLoading: false,
          isError: "Server failed to load the data",
        });
      });
  }

  handleSelectOptionChange = (e) => {
    this.setState({
      region: e.target.value,
    });
  };

  handleChange = (e) => {
    console.log("clicked");
    this.setState({
      searchKeyWord: e.target.value,
    });
  };

  render() {
    if (this.state.isError) {
      return (
        <Box>
          <h1>{this.state.isError}</h1>
        </Box>
      );
    }

    const region = [
      ...this.state.countries.reduce((acc, curr) => {
        acc.add(curr.region);
        return acc;
      }, new Set()),
    ];

    const filtererdDataBySearch =
      this.state.searchKeyWord === ""
        ? this.state.countries
        : this.state.countries.filter((el) =>
            el.name.common
              .toLowerCase()
              .includes(this.state.searchKeyWord.toLocaleLowerCase())
          );

    const filtererdDataByRegion =
      this.state.region === "All"
        ? filtererdDataBySearch
        : filtererdDataBySearch.filter((el) => el.region === this.state.region);

    if (this.state.isLoading) {
      return (
        <Flex h="80vh" direction="column" justify="center" align="center">
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      );
    }

    return (
      <Box bg="RGBA(0, 0, 0, 0.06)">
        <Flex justify="space-between" align="center">
          <Input
            m="2rem 5rem"
            w="30%"
            h="3rem"
            placeholder="Search for a country..."
            _placeholder={{ color: "black" }}
            size="md"
            border="0"
            bg="#fff"
            onChange={this.handleChange}
          />
          <Select
            w="15%"
            m="2rem 5rem"
            h="3rem"
            placeholder="Search by Region"
            border="0"
            bg="#fff"
            onChange={this.handleSelectOptionChange}
          >
            <option value="All">All</option>
            {region.map((value, i) => (
              <option key={i} value={value}>
                {value.toUpperCase()}
              </option>
            ))}
          </Select>
        </Flex>
        <br />

        <Box
          display="grid"
          gridTemplateColumns="repeat(4, 1fr)"
          justifyItems="center"
          w="100vw"
          p="0 3rem"
          gridGap="1rem"
        >
          {filtererdDataByRegion.map((country) => {
            return (
              <Box
                key={country.cca2}
                borderRadius="10px"
                w="80%"
                h="380px"
                m="1rem"
                alignSelf="end"
                border="1px solid grey"
                boxShadow="dark-lg"
              >
                <Box>
                  <Image
                    borderRadius="10px 10px 0 0"
                    w="100%"
                    h="200px"
                    src={country.flags.png}
                    alt={country.name.common}
                  />
                </Box>
                <Box m="1rem">
                  <Heading as="h3" size="lg">
                    <Link to={`/countries/${country.cca2}`}>
                      {country.name.common}
                    </Link>
                  </Heading>
                  <Heading as="h5" size="sm" mTop="10px">
                    Population: {country.population}
                  </Heading>
                  <Heading as="h5" size="sm" mTop="10px">
                    Region: {country.region}
                  </Heading>
                  <Heading as="h5" size="sm" mTop="10px">
                    Capital: {country.capital}
                  </Heading>
                </Box>
              </Box>
            );
          })}
        </Box>
      </Box>
    );
  }
}
