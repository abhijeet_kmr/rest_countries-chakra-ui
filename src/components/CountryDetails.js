import React, { Component } from "react";
import * as CountryAPI from "./fetchCalls";
import { Box, Flex, Image, Button, Text } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { Spinner } from "@chakra-ui/react";

export default class CountryDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: null,
      isLoading: true,
      isError: false,
    };
  }

  async componentDidMount() {
    try {
      const { code } = this.props.match.params;
      // console.log(code);
      const data = await CountryAPI.getCountryByCode(code);
      this.setState({
        country: data[0],
        isLoading: false,
        isError: false,
      });
    } catch (err) {
      this.setState({
        country: null,
        isLoading: false,
        isError: "Could not fetch the data by code",
      });
    }
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.match.params.code !== this.props.match.params.code) {
      try {
        const { code } = this.props.match.params;
        const data = await CountryAPI.getCountryByCode(code);
        this.setState({
          country: data[0],
          isLoading: false,
          isError: false,
        });
      } catch (err) {
        this.setState({
          country: null,
          isLoading: false,
          isError: "failed to load by code",
        });
      }
    }
  }

  render() {
    if (this.state.isError) {
      return (
        <Flex
          height="80vh"
          direction="column"
          justifyContent="center"
          alignItems="center"
        >
          <Text as="h1">{this.state.isError}</Text>
        </Flex>
      );
    }

    if (this.state.isLoading) {
      return (
        <Flex
          height="80vh"
          direction="column"
          justifyContent="center"
          alignItems="center"
        >
          <Spinner
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Flex>
      );
    }

    return (
      <Box height="100vh" bg="RGBA(0, 0, 0, 0.04)">
        <Flex
          w="100vw"
          direction="column"
          justifyContent="center"
          alignItems="center"
        >
          <Box margin="2rem 0" w="90%">
            <Link to="/">
              <Button boxShadow="dark-lg">Back</Button>
            </Link>
          </Box>
          <Flex w="90%" justifyContent="space-between" height="350px">
            <Box w="38%" height="100%">
              <Image
                boxShadow="dark-lg"
                border=".5px solid grey"
                w="100%"
                height="100%"
                src={this.state.country.flags.png}
                alt={this.state.country.name.common}
              />
            </Box>
            <Box w="62%" p="0 2rem">
              <Flex
                justifyContent="space-evenly"
                alignItems="center"
                height="80%"
              >
                <Box>
                  <Text as="b" fontSize="3xl">
                    {this.state.country.name.common}
                  </Text>
                  <Text fontSize="lg" fontWeight="bold">
                    Native Name:{" "}
                    <Text as="span" fontWeight="normal">
                      {this.state.country.name.common}
                    </Text>
                  </Text>
                  <Text fontSize="lg" fontWeight="bold">
                    Population:{" "}
                    <Text as="span" fontWeight="normal">
                      {this.state.country.population}
                    </Text>
                  </Text>
                  <Text fontSize="lg" fontWeight="bold">
                    Region:{" "}
                    <Text as="span" fontWeight="normal">
                      {this.state.country.region}
                    </Text>
                  </Text>{" "}
                  <Text fontSize="lg" fontWeight="bold">
                    Sub Region:{" "}
                    <Text as="span" fontWeight="normal">
                      {this.state.country.subregion}
                    </Text>
                  </Text>{" "}
                  <Text fontSize="lg" fontWeight="bold">
                    Capital:{" "}
                    <Text as="span" fontWeight="normal">
                      {this.state.country.capital}
                    </Text>
                  </Text>
                </Box>
                <br />
                <Box>
                  <Text fontSize="lg" fontWeight="bold">
                    Top Level Domain:{" "}
                    <Text as="span" fontWeight="normal">
                      {this.state.country.tld}
                    </Text>
                  </Text>
                  <Text fontSize="lg" fontWeight="bold">
                    Currencies:{" "}
                    {Object.values(this.state.country.currencies).map(
                      (el, i) => {
                        return (
                          <Text key={i} as="span" fontWeight="normal">
                            {el.name}
                          </Text>
                        );
                      }
                    )}
                  </Text>
                  <Text fontSize="lg" fontWeight="bold">
                    Languages:{" "}
                    {Object.keys(this.state.country.languages).map((el) => {
                      return (
                        <Text key={el} as="span" fontWeight="normal">
                          {this.state.country.languages[el]}&nbsp;
                        </Text>
                      );
                    })}
                  </Text>
                </Box>
              </Flex>
              <Box p="0 5rem">
                {this.state.country.borders &&
                this.state.country.borders.length ? (
                  <Text fontSize="lg" fontWeight="bold">
                    Border Countries:
                    {this.state.country.borders?.map((border) => {
                      return (
                        <Link key={border} to={`/countries/${border}`}>
                          <Button boxShadow="dark-lg" margin="2px 5px">
                            {border}
                          </Button>
                        </Link>
                      );
                    })}
                  </Text>
                ) : null}
              </Box>
            </Box>
          </Flex>
        </Flex>
      </Box>
    );
  }
}
