import React, { Component } from "react";
import Header from "./components/Header";
import Content from "./components/Content";
import CountryDetails from "./components/CountryDetails";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";

export default class App extends Component {
  render() {
    return (
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Content} />
          <Route path="/countries/:code" component={CountryDetails} />
        </Switch>
      </Router>
    );
  }
}
